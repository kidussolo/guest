const validator = require("validator");
const isEmpty = require("./is-empty");
module.exports = function validateLoginInput(data) {
  //first create empty error
  let errors = {};
  //check string name of if empty string

  data.name = !isEmpty(data.name) ? data.name : "";
  data.password = !isEmpty(data.password) ? data.password : "";

  //check for the name
  if (validator.isEmpty(data.name)) {
    errors.name = "name is required";
  }

  //check for the password
  if (validator.isEmpty(data.password)) {
    errors.password = "password is required";
  }

  return {
    errors,
    isvalid: isEmpty(errors)
  };
};
