const validator = require("validator");
const isEmpty = require("./is-empty");
module.exports = function validateGuestInput(data) {
  //first create empty error
  let errors = {};
  //check string name of if empty string

  data.fullName = !isEmpty(data.name) ? data.name : "";
  data.city = !isEmpty(data.city) ? data.city : "";
  data.subCity = !isEmpty(data.subCity) ? data.subCity : "";
  data.woreda = !isEmpty(data.woreda) ? data.woreda : "";
  data.houseNumber = !isEmpty(data.houseNumber) ? data.houseNumber : "";
  data.phoneNumber = !isEmpty(data.phoneNumber) ? data.phoneNumber : "";
  data.idNumber = !isEmpty(data.idNumber) ? data.idNumber : "";
  data.purpose = !isEmpty(data.purpose) ? data.purpose : "";

  //check for the fullName
  if (validator.isEmpty(data.fullName)) {
    errors.fullName = "Full Name is required";
  }

  //check for the city
  if (validator.isEmpty(data.city)) {
    errors.city = "City is required";
  }

  //check for the subCity
  if (validator.isEmpty(data.subCity)) {
    errors.subCity = "Sub City is required";
  }

  //check for the woreda
  if (validator.isEmpty(data.woreda)) {
    errors.woreda = "Woreda is required";
  }

  //check for the houseNumber
  if (validator.isEmpty(data.houseNumber)) {
    errors.houseNumber = "House Number is required";
  }
  //check for the phoneNumber
  if (validator.isEmpty(data.phoneNumber)) {
    errors.phoneNumber = "Phone Number is required";
  }

  //check for the phoneNumber
  if (!validator.isNumeric(data.phoneNumber)) {
    errors.phoneNumber = "Invalid Phone Number";
  }

  //check for the idNumber
  if (validator.isEmpty(data.idNumber)) {
    errors.idNumber = "Id Number is required";
  }

  //check for the purpose
  if (validator.isEmpty(data.purpose)) {
    errors.purpose = "Purpose of stay is required";
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
