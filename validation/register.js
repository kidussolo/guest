const validator = require("validator");
const isEmpty = require("./is-empty");
module.exports = function validateRegisterInput(data) {
  //first create empty error
  let errors = {};
  //check for conditions on the forms

  //check string name of if empty string
  console.log("erros before " + errors);

  data.name = !isEmpty(data.name) ? data.name : "";
  data.password = !isEmpty(data.password) ? data.password : "";
  data.password2 = !isEmpty(data.password2) ? data.password2 : "";
  data.role = !isEmpty(data.role) ? data.role : "";

  //check for the name
  if (validator.isEmpty(data.name)) {
    errors.name = "name is required";
  }

  //check for the password
  if (validator.isEmpty(data.password)) {
    errors.password = "password is required";
  }

  //check for the role
  if (validator.isEmpty(data.role)) {
    errors.role = "role is required";
  }

  //check for the password length
  if (!validator.isLength(data.password, { min: 6, max: 20 })) {
    errors.passwordLength = "password must be at least 6 characters";
  }

  //for the confirm password
  if (validator.isEmpty(data.password2)) {
    errors.password2 = "Confirm password is required";
  }

  //for the confirm password
  if (!validator.equals(data.password, data.password2)) {
    errors.passwordMatch = "Passwords must Match";
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
