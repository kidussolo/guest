Guest House Management system for a Guest house manager built using node express backend and react front end with mongodb database to store data

## Quick Start

need to have mongodb installed on your machine

```bash
# Install dependencies for server
npm install

# Install dependencies for client
npm run client-install

# Run the client & server with concurrently
npm run dev

# Run the Express server only
npm run server

# Run the React client only
npm run client

You will need to create a keys.js in the server config folder with

```
module.exports = {
  mongoURI: 'YOUR_OWN_MONGO_URI',
  secretOrKey: 'YOUR_OWN_SECRET'
};

##Author kidus solomon