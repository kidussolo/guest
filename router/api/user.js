const express = require("express");
const router = express.Router();
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const passport = require("passport");

//load input validation
const validateLoginInput = require("../../validation/login");
const validateRegisterInput = require("../../validation/register");
const User = require("../../models/User");
const key = require("../../config/keys");

// @route GET api/users/register
// @desc  register users
// @access public
router.post("/register", (req, res) => {
  const { errors, isValid } = validateRegisterInput(req.body);

  //check validation
  if (!isValid) {
    return res.status(400).json(errors);
  }

  const newUser = new User({
    name: req.body.name,
    role: req.body.role,
    password: req.body.password
  });
  bcrypt.genSalt(10, (err, salt) => {
    bcrypt.hash(newUser.password, salt, (err, hash) => {
      if (err) throw err;
      newUser.password = hash;
      newUser
        .save()
        .then(user => res.json(user))
        .catch(err => console.log(err));
    });
  });
});

// @route POST api/users/login
// @desc  login users
// @access public
router.post("/login", (req, res) => {
  const { errors, isValid } = validateLoginInput(req.body);

  //check validation
  if (!isValid) {
    return res.status(400).json(errors);
  }

  const name = req.body.name;
  const password = req.body.password;

  User.findOne({ name }).then(user => {
    //Check if user exists
    console.log(user);
    if (!user) {
      errors.name = "User not found";
      return res.status(404).json(errors);
    }

    //Check if password matches
    bcrypt.compare(password, user.password).then(isMatch => {
      if (isMatch) {
        //user match

        const payload = {
          id: user.id,
          name: user.name,
          role: user.role
        };

        //sign
        jwt.sign(
          payload,
          key.secretOrKey,
          { expiresIn: 3600 },
          (err, token) => {
            res.json({
              success: true,
              token: "Bearer " + token
            });
          }
        );
      } else {
        errors = "password incorrect";
        return res.status(400).json(errors);
      }
    });
  });
});

// @route GET api/users/register
// @desc  register users
// @access public
router.get(
  "/current",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    res.json({
      id: req.user.id,
      name: req.user.name,
      role: req.user.role
    });
  }
);
module.exports = router;
