const express = require("express");
const router = express();
const mongoose = require("mongoose");
const Guest = require("../../models/Guest");
const passport = require("passport");

const validateGuestInput = require("../../validation/guest");

// @route POST api/guest/newGuest
// @desc  Inserts new Guest
// @access private
router.post("/newGuest", (req, res) => {
  const { errors, isValid } = validateGuestInput(req.body);

  //check validation
  if (!isValid) {
    return res.status(400).json(errors);
  }

  const newGuest = new Guest({
    fullName: req.body.fullName,
    city: req.body.city,
    subCity: req.body.subcity,
    woreda: req.body.woreda,
    houseNumber: req.body.houseNumber,
    phoneNumber: req.body.phoneNumber,
    idNumber: req.body.idNumber,
    purpose: req.body.purpose,
    date: Date.now()
  });

  newGuest
    .save()
    .then(guest => res.json(guest))
    .catch(err =>
      res
        .status(500)
        .send({ error: "Some error occurred while creating a new Guest" })
    );
});

// @route GET api/guest/guests
// @desc  Get all guests registered
// @access public
router.get("/guests", (req, res) => {
  Guest.find()
    .then(guests => res.json(guests))
    .catch(err => console.log(err));
});

// @route GET api/guest/guest
// @desc  Get only one registed users
// @access public
router.get("/guest/:id", (req, res) => {
  const id = req.params.id;
  Guest.findById(id)
    .then(guest => res.json(guest))
    .catch(err => console.log(err));
});

// @route GET api/guest/guest
// @desc  Get only one registed users by full name
// @access public
router.get("/guest/fullName", (req, res) => {
  const id = req.params.fullName;
  const db = req.db;
  db.guests
    .find({ fullName: req.params.fullName })
    //Guest.findById(id)
    .then(guest => res.json(guest))
    .catch(err => console.log(err));
});

// @route PUT api/guest/updateGuest
// @desc  Updates a guest information
// @access public
router.put("/guest/:id", (req, res, next) => {
  const id = req.params.id;
  Guest.findById(id).then(guest => {
    guest.fullName = req.body.fullName;
    guest.city = req.body.city;
    guest.subCity = req.body.subcity;
    guest.woreda = req.body.woreda;
    guest.houseNumber = req.body.houseNumber;
    guest.phoneNumber = req.body.phoneNumber;
    guest.idNumber = req.body.idNumber;
    guest.purpose = req.body.purpose;
    guest.date = Date.now();

    guest.save().then(guest =>
      res.send({
        message: "success",
        guest
      })
    );
  });
});

// @route DELETE api/guest/deleteGuest
// @desc  Delete only one registed users
// @access public
router.delete("/guest/:id", (req, res) => {
  const id = req.params.id;
  Guest.findByIdAndRemove(id)
    .then(guest => {
      res.send({
        message: "record deleted successfully",
        guest
      });
    })
    .catch(err => console.log(err));
});

// @route DELETE api/guest/deleteAllGuest
// @desc  Delete all registered guests
// @access public
router.delete("/guests/deleteAll", (req, res) => {
  Guest.remove()
    .then(guest => {
      res.send({
        message: "All records deleted successfuly"
      });
    })
    .catch(err => console.log(err));
});

module.exports = router;
