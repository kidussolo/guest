import React, { Component } from "react";
import { Link } from "react-router-dom";
import axios from "axios";

class Guest extends Component {
  constructor() {
    super();
    this.state = {
      fullName: "",
      city: "",
      subCity: "",
      woreda: "",
      houseNumber: "",
      phoneNumber: "",
      idNumber: "",
      purpose: "",
      errors: {}
    };
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  onSubmit(e) {
    e.preventDefault();
    const newUser = {
      name: this.state.name,
      role: this.state.role,
      password: this.state.password,
      password2: this.state.password2
    };

    axios
      .post("/api/users/register", newUser)
      .then(res => console.log(res.data))
      .catch(err => console.log(err.response.data));
  }

  render() {
    return (
      <section className="container">
        <h1 className="large text-primary">Add Guest</h1>
        <p className="lead">
          <i className="fas fa-user" /> Add new Guest Information
        </p>
        <form onSubmit={this.onSubmit} className="form">
          <div className="form-group">
            <input
              type="text"
              placeholder="Full Name"
              required
              name="fullName"
              value={this.state.fullName}
              onChange={this.onChange}
            />
          </div>
          <div className="form-group">
            <input
              type="text"
              placeholder="City"
              required
              name="city"
              value={this.state.city}
              onChange={this.onChange}
            />
          </div>
          <div className="form-group">
            <input
              type="text"
              placeholder="Sub City"
              required
              name="subCity"
              value={this.state.subCity}
              onChange={this.onChange}
            />
          </div>
          <div className="form-group">
            <input
              type="text"
              placeholder="Woreda"
              name="woreda"
              value={this.state.woreda}
              onChange={this.onChange}
            />
          </div>
          <div className="form-group">
            <input
              type="text"
              placeholder="house Number"
              name="houseNumber"
              value={this.state.houseNumber}
              onChange={this.onChange}
            />
          </div>
          <div className="form-group">
            <input
              type="text"
              placeholder="phone Number"
              name="phoneNumber"
              value={this.state.phoneNumber}
              onChange={this.onChange}
            />
          </div>
          <div className="form-group">
            <input
              type="text"
              placeholder="Id Or Passport Number"
              name="idNumber"
              value={this.state.idNumber}
              onChange={this.onChange}
            />
          </div>
          <div className="form-group">
            <input
              type="text"
              placeholder="Purpose Of Stay"
              name="purpose"
              value={this.state.purpose}
              onChange={this.onChange}
            />
          </div>
          <input type="submit" value="Add" className="btn btn-primary" />
        </form>
      </section>
    );
  }
}

export default Guest;
