import React, { Component } from "react";
import { Link } from "react-router-dom";

class Main extends Component {
  render() {
    return (
      <section className="landing">
        <div className="dark-overlay">
          <div className="landing-inner">
            <h1 className="x-large">Sameek GuestHouse</h1>
            <p className="lead">
              Easy Guest House registration and retrieval management system
            </p>
            <div className="buttons">
              <Link to="/register" className="btn btn-primary">
                Sign Up
              </Link>
              <Link to="/login" className="btn btn">
                Login
              </Link>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default Main;
