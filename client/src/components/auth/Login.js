import React, { Component } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import { withRouter } from "react-router-dom";
import PropTypes from "prop-types";
import classnames from "classnames";
import { connect } from "react-redux";
import { userLogin } from "../../actions/authAction";

class Login extends Component {
  constructor() {
    super();
    this.state = {
      name: "",
      password: "",
      errors: {}
    };
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  //set errors to the component state
  componentwillRecieveProps(nextProps) {
    if (nextProps.errors) {
      this.setState({ errors: this.nextProps.errors });
    }
  }

  onSubmit(e) {
    e.preventDefault();
    const user = {
      name: this.state.name,
      password: this.state.password
    };
    // axios
    //   .post("/api/users/login", user)
    //   .then(res => console.log(res.data))
    //   .catch(err => this.setState({ errors: err.response.data }));

    this.props.registeruser(newUser, this.props.history);
  }

  render() {
    const { errors } = this.state;
    return (
      <section className="container">
        <h1 className="large text-primary">Sign In</h1>
        <p className="lead">
          <i className="fas fa-user" /> Sign into your account
        </p>
        <form onSubmit={this.onSubmit} className="form">
          <div className="form-group">
            <input
              className={classnames({
                "is-invalid": errors.password
              })}
              type="text"
              placeholder="User Name"
              name="name"
              value={this.state.name}
              onChange={this.onChange}
            />

            {errors.name && (
              <div className="invalid-feedback">{errors.name}</div>
            )}
          </div>
          <div className="form-group">
            <input
              type="password"
              placeholder="Password"
              minLength="6"
              name="password"
              value={this.state.password}
              onChange={this.onChange}
            />
          </div>
          <input type="submit" value="Login" className="btn btn-primary" />
        </form>
        <p className="my-1">
          Don't have an account? <Link to="/register">Sign Up</Link>
        </p>
      </section>
    );
  }
}

Register.propTypes = {
  userLogin: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.auth
});

export default connect(mapStateToProps, { userLogin })(withRouter(Login));
