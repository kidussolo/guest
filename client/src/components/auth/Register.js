import React, { Component } from "react";
import { Link } from "react-router-dom";
import { withRouter } from "react-router-dom";
import PropTypes from "prop-types";
import classnames from "classnames";
import { connect } from "react-redux";
import { registeruser } from "../../actions/authAction";

class Register extends Component {
  constructor() {
    super();
    this.state = {
      name: "",
      role: "",
      password: "",
      password2: "",
      errors: {}
    };
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  //set errors to the component state
  componentwillRecieveProps(nextProps) {
    if (nextProps.errors) {
      this.setState({ errors: this.nextProps.errors });
    }
  }

  onSubmit(e) {
    e.preventDefault();
    const newUser = {
      name: this.state.name,
      role: this.state.role,
      password: this.state.password,
      password2: this.state.password2
    };
    //any action is called through props
    this.props.registeruser(newUser, this.props.history);
  }

  render() {
    const { errors } = this.state;
    return (
      <section className="container">
        <h1 className="large text-primary">Sign Up</h1>
        <p className="lead">
          <i className="fas fa-user" /> Create Your Account
        </p>
        <form onSubmit={this.onSubmit} className="form">
          <div className="form-group">
            <input
              type="text"
              placeholder="Name"
              required
              name="name"
              value={this.state.name}
              onChange={this.onChange}
            />
          </div>
          <div className="form-group">
            <input
              type="text"
              placeholder="Role"
              required
              name="role"
              value={this.state.role}
              onChange={this.onChange}
            />
          </div>
          <div className="form-group">
            <input
              type="password"
              placeholder="Password"
              minLength="6"
              name="password"
              value={this.state.password}
              onChange={this.onChange}
            />
          </div>
          <div className="form-group">
            <input
              type="password"
              placeholder="Confirm Password"
              minLength="6"
              name="password2"
              value={this.state.password2}
              onChange={this.onChange}
            />
          </div>
          <input type="submit" value="Register" className="btn btn-primary" />
        </form>
        <p className="my-1">
          Already have an account? <Link to="/login">Sign In</Link>
        </p>
      </section>
    );
  }
}
//mapping props to property types
Register.propTypes = {
  registeruser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};

//mapping state to property
const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});

export default connect(mapStateToProps, { registeruser })(withRouter(Register));
