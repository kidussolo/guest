import axios from "axios";

const setAuthToken = token => {
  if (token) {
    //apply the token to every the header of every request
    axios.defaults.headers.common["Authorization"] = token;
  }
  //if token is not present delete the header
  delete axios.defaults.headers.common["Authorization"];
};

export default setAuthToken;
