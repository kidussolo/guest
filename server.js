const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const passport = require("passport");

const app = express();

//body parser middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//connection string
const db = require("./config/keys").mongoURI;

//routes for
const users = require("./router/api/user");
const guest = require("./router/api/guest");

//Connect to Database
mongoose
  .connect(db)
  .then(() => console.log("Datebase Connected"))
  .catch(err => console.log(err));

//passport middleware
app.use(passport.initialize());

//passport config
require("./config/passport")(passport);

app.use("/api/users", users);
app.use("/api/guest", guest);

const port = process.env.PORT || 5000;

app.listen(port, () => console.log(`server running on port ${port}`));
