const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const GuestSchema = new Schema({
  fullName: {
    type: String,
    required: true
  },
  city: {
    type: String,
    requires: true
  },
  subCity: {
    type: String,
    requires: true
  },
  woreda: {
    type: Number,
    requires: true
  },
  houseNumber: {
    type: Number,
    requires: true
  },
  phoneNumber: {
    type: Number,
    requires: true
  },
  idNumber: {
    type: Number,
    requires: true
  },
  purpose: {
    type: String,
    requires: true
  },
  date: {
    type: Date,
    default: Date.now
  }
});

const Guest = mongoose.model("guest", GuestSchema);
module.exports = Guest;
